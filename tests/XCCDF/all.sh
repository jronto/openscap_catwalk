#!/bin/sh

. $srcdir/../test_common.sh

test_init test_xccdf_resolution_tests.log
test_run "Inheritence test (comparing resolved and unresolved xmls)" ./test_xccdf_resolve_comparison.sh $srcdir/profile_resolving_test-xccdf.xml
#test_run "Inheritence test (resolving inherited branches and leaves of xmls)" ./test_xccdf_resolve_leaves.sh $srcdir/profile_resolving_test-xccdf.xml
test_exit
