#!/usr/bin/env bash

# Author: Jennie Ronto <jronto@redhat.com>
# OpenScap Test Suite


# init
. ../test_common.sh

export OSCAP_FULL_VALIDATION=1
export vrb=0
export cln=0

version="0.0.1"
this=`basename $0`




# functions
usage()
{
    echo "Usage: $this [-h|V]"
    echo "       $this [-v[v]] unresolved_xccdf.xml [unresolved_xccdf.xml ...]"
    [ "$cln" -eq 1 ] && cleanup; 
    exit 1;
}

cleanup()
{
    dbg 1 "$FUNCNAME"
    rm -f "$res_xccdf" 
    export cln=0
}

## test cases 
resolv_check()
{
    dbg 1 "$FUNCNAME"

    dbg 2 "processing file $1"
    grep -qE '^<Benchmark.*resolved="1"' "$1" 2>/dev/null
    case "$?" in 
        0)  warnmsg "\`$1""' is already resolved. Skipping.";;
        1)  resolv_xccdf "$1" ;;
        2)  errmsg "File not found or inaccessible: \`$1'";;
        *)  errmsg "grep exitex with code $? on file: \`$1'"
    esac
}

unallowed_attributes()
{
    dbg 1 "$FUNCNAME"
    
    grep -Eq '@extends|@abstract' "$1" 2>/dev/null; 
    case "$?" in
        1 ) dbg 2 "passed $FUNCNAME test."
            return 0;;
        2)  errmsg "File not found or inaccessible: \`$1'";;
        * ) errmsg "Resolved XCCDF sholud contain no '@extends' or '@abstract' attributes" ;;
    esac
}

shouldhave_attribute()
{
    dbg 1 "$FUNCNAME"
    
    grep -Eq '^<Benchmark.*resolved="1"' "$1" 2>/dev/null; 
    case "$?" in
        0 ) dbg 2 "passed $FUNCNAME test."
            return 0;;
        2)  errmsg "File not found or inaccessible: \`$1'";;
        * ) errmsg "Resolved XCCDF sholud contain a 'resolved=\"1\"' attribute" ;;
    esac

}

resolv_xccdf()
{
    dbg 1 "$FUNCNAME"
    
    raw_xccdf="$1"
    res_xccdf=`mktemp`
    export cln=1
    
    oscap xccdf resolve --force --output "$res_xccdf" "$raw_xccdf"
    
    unallowed_attributes "$res_xccdf"; rv_ua="$?" 
    shouldhave_attribute "$res_xccdf"; rv_sa="$?" 

    case "$rv_ua$rv_sa" in
        00 ) pass "$1"
            [ "$cln" -eq 1 ] && cleanup
            ;;
        * ) errmsg "Smoke tests have failed.";;
    esac
}




#### MAIN #### 

if [ "$#" -eq "1" ]; then
    case "$1" in
        -h | --help ) usage ;;
        -V | --version ) echo "Version: $version"; exit 0 ;;
        - | -?* ) errmsg 1 "Illegal standalone flag: \"$1\". Please specify parameters accordingy." ;; 
        * ) resolv_check "$1" ;;
    esac

elif [ "$#" -gt "1" ]; then 

    while [ "$1" != "" ]
    do
        case "$1" in
            -v | --verbose ) export vrb=$((++vrb)) ;;
            -vv ) export vrb=$((vrb+2)) ;;
            - | -?* ) errmsg 1 "Illegal flag or context: \"$1\". Please specify parameters accordingly." ;; 
            * ) resolv_check "$1" ;;
        esac
        shift
    done

else errmsg 1 "No flags or insufficient parameters have been specified." 
fi












