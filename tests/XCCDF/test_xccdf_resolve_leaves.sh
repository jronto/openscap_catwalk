#!/usr/bin/env bash

# Author: Jennie Ronto <jronto@redhat.com>
# OpenScap Test Suite

# init
. ../test_common.sh

export OSCAP_FULL_VALIDATION=1
export vrb=0
export cln=0

version="0.0.1"
this=`basename $0`


# functions
usage()
{
    echo "Usage: $this [-h|V]"
    echo "       $this [-v[v]] unresolved_xccdf.xml [unresolved_xccdf.xml ...]"
    exit 1;
}

cleanup()
{
    dbg 1 "$FUNCNAME"
    rm "$res_xccdf"
}

pm_append()
{
    dbg 1 "$FUNCNAME"
}


pm_override()
{
    #dbg 1 "$FUNCNAME"

    echo "$1" | grep -q '@override=true' 
    if [ "$?" -eq 0 ]; then
        echo '@override=true' 
    elif [ "$?" -eq 1 ]; then
        pm_append "$1"
    fi 
}


## test cases 
resolv_check()
{
    dbg 1 "$FUNCNAME"
    dbg 2 "processing file $1"

    grep -qE '^<Benchmark.*resolved="1"' "$1" 2>/dev/null
    case "$?" in 
        0)  warnmsg "\`$1""' is already resolved. Skipping.";;
        1)  resolv_xccdf "$1" ;;
        2)  errmsg "File not found or inaccessible: \`$1'";;
        *)  errmsg "grep exitex with code $? on file: \`$1'"
    esac
}

resolv_xccdf()
{
    dbg 1 "$FUNCNAME"
    
    checkstring=""
    
    raw_xccdf="$1"
    res_xccdf=`mktemp`
    export cln=1
    
    oscap xccdf resolve --force --output "$res_xccdf" "$raw_xccdf"
    #res_xccdf=tmp.9ZtON6SQeF #debug: this should be a valid output

    
    profile_cnt=`xpath "$res_xccdf" 'count(//Profile)' 2>/dev/null`
    dbg 2 "sum profiles: $profile_cnt"
    
    ext_cnt=`xpath "$raw_xccdf" "count(//Profile/@extends)" 2>/dev/null`
    dbg 2 "sum entensions: $ext_cnt"
    echo "======================" 
    
    for i in `seq "$profile_cnt"`
    do
        ext_curr=`xpath "$raw_xccdf" "//Profile[$i]/@extends" 2>/dev/null | cut -f2 -d'"'`
        profile_raw=`xpath "$raw_xccdf" "//Profile[$i]/@id" 2>/dev/null | cut -f2 -d'"'`        
        
        dbg 2 "profile $i: $profile_raw"
        
        for j in "$raw_xccdf" "$res_xccdf" 
        do
            subnode_cnt=`xpath "$j" "count(//Profile[$i]/*)" 2>/dev/null`
            dbg 2 "subnodes in \"$j\": $subnode_cnt"
        done    
        
        if [ -z "$ext_curr" ]; then    
            dbg 2 "POTENCIAL PARENT PROFILE (not subject to inheritence)"
        else
            dbg 2 "contains extension: [$ext_curr]"

            # this is just some output for debugging to see what the extended profile conatins BEFORE extension
            #dbg 2 "___ORIGINAL___ conains"
            #echo -n "   "; xpath "$raw_xccdf" "//Profile[@id=\"$profile_raw\"]" 2>/dev/null; echo
           
            # this is just some output for debugging to see what the extended profile SHOULD contain
            #dbg 2 "___EXTEND WITH___ conains"
            #echo -n "   "; xpath "$raw_xccdf" "//Profile[@id=\"$ext_curr\"]" 2>/dev/null; echo
           
            # this is just some output for debugging to see what the extended profile DOES contain in the resolved xccdf
            #dbg 2 "___RESULT___ contains"
            #echo -n "   "; xpath "$res_xccdf" "//Profile[@id=\"$profile_raw\"]" 2>/dev/null; echo
            

            # idea: loop through the unresolved profile, extend it accordingly, and compare it with the resolved file to see if they are the same. 
            while read query
            do
                case "$query" in
                    *\<abstract*|*\<cluster-id*|*\<extends*|*\<id*|*\<signature*|*\<status*|*\<dc-status* ) 
                        echo "pm_none" ;;
                    *\<source*|*\<choices* ) 
                        echo "pm_prepend" ;;
                    *\<requires*|*\<conflicts*|*\<ident*|*\<fix*|*\<value*|*\<complex-value*|*\<default*|*\<complex-default*|*\<lower-bound*|*\<upper-bound*|*\<match*|*\<select*|*\<refine-value*|*\<refine-rule*|*\<set-value*|*\<set-complex-value*|*\<profile-note* ) 
                        pm_append "$query" 
                        ;;
                    *\<hidden*|*\<prohibitChanges*|*\<selected*|*\<version*|*\<weight*|*\<operator*|*\<interfaceHint*|*\<check*|*\<complex-check*|*\<role*|*\<severity*|*\<type*|*\<interactive*|*\<multiple*|*\<note-tag*|*\<impact-metric* ) 
                        echo "pm_replace" ;;
                    *\<title*|*\<description*|*\<platform*|*\<question*|*\<rationale*|*\<warning*|*\<reference*|*\<fixtext* ) 
                        #pm_override "$query" 
                        case "$query" in
                            *\<title* ) 
                                old_title=`echo "$query" | sed 's:<title>\|/>\|</title>::g'`
                                new_title=`xpath "$raw_xccdf" "//Profile[@id=\"$ext_curr\"]/title/text()" 2>/dev/null`
                                appended_title="$old_title$new_title"
                                ogen_title=`xpath "$res_xccdf" "//Profile[@id=\"$profile_raw\"]/title/text()" 2>/dev/null`
                                if [[ "$appended_title" = "$ogen_title" ]]; then 
                                    dbg 2 "title ok"
                                else 
                                    checkstring="$checkstring""1"
                                    warnmsg "title incorrectly inherited ($profile_raw)"
                                fi
                                ;;
                             * ) 
                               tag=`echo "$query" | sed 's[<\|>\| [:[g' | cut -f2 -d':' `
                               new_elements=`xpath "$raw_xccdf" "//Profile[@id=\"$ext_curr\"]/$tag" 2>/dev/null`
                               appended_elements="$query$new_elements"
                               
                               extraction=""
                               for k in `seq 24`
                               do
                                    extraction="$extraction"`echo "$appended_elements" | sed "s/<\|>/$tag/g" | cut -f "$k" -d'"' | grep -v "$tag" | tr -d '\n'`
                               done
                               
                               if [[ "$extraction" = `xpath "$res_xccdf" "//Profile[@id=\"$profile_raw\"]/$tag/text()" 2>/dev/null;` ]]; then
                                dbg 2 "appended tags ok"
                               else 
                                checkstring="$checkstring""1" 
                                warnmsg "appended properties incorrectly inherited ($profile_raw)"
                               fi
                                ;;
                        esac
                        ;;
                esac
            done < <( xpath "$raw_xccdf" "//Profile[@id=\"$profile_raw\"]" 2>/dev/null) 
            
            # todo: check loans (properties that are only in the "extended with" profile
        fi 
        
        dbg 2 "----------------------" 
    done
    
    if [[ -n "$checkstring" ]]; then
        errmsg "Incorrect inheritence has been found!"
    else
        pass "Leaf test OK"
    fi
}




#### MAIN #### 

if [ "$#" -eq "1" ]; then
    case "$1" in
        -h | --help ) usage ;;
        -V | --version ) echo "Version: $version"; exit 0 ;;
        - | -?* ) errmsg 1 "Illegal standalone flag: \"$1\". Please specify parameters accordingy." ;; 
        * ) resolv_check "$1" ;;
    esac

elif [ "$#" -gt "1" ]; then 

    while [ "$1" != "" ]
    do
        case "$1" in
            -v | --verbose ) export vrb=$((++vrb)) ;;
            -vv ) export vrb=$((vrb+2)) ;;
            - | -?* ) errmsg 1 "Illegal flag or context: \"$1\". Please specify parameters accordingly." ;; 
            * ) resolv_check "$1" ;;
        esac
        
        shift
    done

else errmsg 1 "No flags or insufficient parameters have been specified." 
fi

