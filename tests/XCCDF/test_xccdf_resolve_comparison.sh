#!/usr/bin/env bash

# Author: Jennie Ronto <jronto@redhat.com>
# OpenScap Test Suite


# init
. ../test_common.sh

export OSCAP_FULL_VALIDATION=1
export vrb=0
export cln=0

version="0.1.2"
this=`basename $0`



# functions
usage()
{
    echo "Usage: $this [-h|V]"
    echo "       $this [-v[v]] unresolved_xccdf.xml [unresolved_xccdf.xml ...]"
    exit 1;
}

cleanup()
{
    dbg 1 "$FUNCNAME"
    rm -f "$res_xccdf" "$raw_attribs" "$res_attribs" "$dif_attribs" "$raw_names" "$res_names" "$dif_names"
    export cln=0 
}

## test cases 
resolv_check()
{
    dbg 1 "$FUNCNAME"

    dbg 2 "processing file $1"
    grep -qE '^<Benchmark.*resolved="1"' "$1" 2>/dev/null
    case "$?" in 
        0)  warnmsg "\`$1""' is already resolved. Skipping.";;
        1)  resolv_xccdf "$1" ;;
        2)  errmsg "File not found or inaccessible: \`$1'";;
        *)  errmsg "grep exitex with code $? on file: \`$1'"
    esac
}

process_diff()
{
# usage: diffile [cmpfile]

    dbg 1 "$FUNCNAME"
    extscore=0
    namescore=0
    attrcnt=0
    incnt=0
    
    while read line;
    do
        case "$line" in
            \<* )   str_out=${line:2}
                    dbg 2 "str_out $str_out"
                    
                    if [[ "$str_out" = extends* ]]; then
                        extscore=$((++extscore))
                        attrcnt=0
                        incnt=0
                    fi
                ;;
                
            \>* )   str_in=${line:2}
                    dbg 2 "str_in $str_in"
                    
                    if [ "$#" -eq 2 ] && [[ "$str_in" = *"$str_out"* ]]; then 
                            errmsg "Profile name should be overridden."
                            return 1;

                    elif [ "$#" -eq 3 ]; then
                            #dbg 2 "attr str_in $str_in"
                        [ "$attrcnt" -eq 0 ] && str_prev="$str_out"
                        
                        a=`grep -B1 "$str_in" "$2" | head -n1 | cut -f2 -d'='` 
                        b=`echo $str_prev | cut -f2 -d'='`
                        # there's no telling how many attributes there will be, 
                        #  but as soon as one does not match, the inheritence is surely erroneus
                        [ "$a" != "$b" ] && [ "$extscore" -ge 1 ] && extscore=$((--extscore)) 
                            #dbg 2 "$a $b"
                        
                        str_prev="$str_in"
                        attrcnt=$((++attrcnt))
                        incnt=$((++incnt))
                     
                    else warnmsg "Incorrect profile inheritence in $1"
                    fi
                ;;

            * ) errmsg "Error while processing diff file $1";;
        esac 
        
    done < "$2"

#    if [ "$#" -eq 2 ]; then
#        dbg 2 "namescore $namescore"
#        return "$namescore"
#    else
#        dbg 2 "extscore $extscore"
#        return "$extscore"
#    fi 

    if [ "$incnt" -eq "$attrcnt" ]; then
        return 0;
    else
        dbg 2 "in string count: $incnt"
        dbg 2 "attribute count: $attrcnt"
        return 1;
    fi

}


resolv_xccdf()
{
    dbg 1 "$FUNCNAME"
    
    raw_xccdf="$1"
    res_xccdf=`mktemp`
    raw_attribs=`mktemp`
    res_attribs=`mktemp`
    dif_attribs=`mktemp`
    raw_names=`mktemp`
    res_names=`mktemp`
    dif_names=`mktemp`
    export cln=1
    
    
    
    #raw_xccdf='profile_resolving_test1-xccdf.xml';  # REMOVE ME
    #oscap xccdf resolve --force --output "$res_xccdf" "$raw_xccdf"
    res_xccdf=tmp.9ZtON6SQeF #debug: this should be a valid output
    #res_xccdf='resolved_good.xml';                  # REMOVE ME
    #res_xccdf=/tmp/tmp.5UxjZpLJdu                   # REMOVE ME
    
    xpath "$raw_xccdf" '/descendant-or-self::Profile//text()' 2>/dev/null >> "$raw_names"
    xpath "$res_xccdf" '/descendant-or-self::Profile//text()' 2>/dev/null >> "$res_names"

    for i in `xpath "$raw_xccdf" '/descendant-or-self::Profile//@*' 2>/dev/null`; do echo "$i"; done >> "$raw_attribs" 
    for i in `xpath "$res_xccdf" '/descendant-or-self::Profile//@*' 2>/dev/null`; do echo "$i"; done >> "$res_attribs"

    ## diffs
    diff -Bbw "$raw_names" "$res_names" | grep -E '^<|^>' | tr -s ' ' >> "$dif_names"
        #< Profile B
        #> Profile AProfile B
    
    dbg 2 '------------'
    diff -Bbw "$raw_attribs" "$res_attribs" | grep -E '^<|^>' | grep -vE '^> xml:lang="[a-zA-Z]+"$|^> hidden="[tf].+"$' | tee -a "$dif_attribs"
        #< extends="xccdf_cdf_profile_A"
        #> idref="xccdf_cdf_rule_1"
        #> selected="false"
        
    process_diff "$raw_xccdf" "$dif_names"; n="$?"
    process_diff "$raw_xccdf" "$dif_attribs" "$raw_attribs"; e="$?"
    #dbg 2 "namescore $n extension score $e"
           
    #if [ "$n" -eq "$e" ]; then 
    if [[ "$n$e" = '00' ]]; then
        [ "$cln" -eq 1 ] && cleanup; 
        pass "$raw_xccdf"
        return 0
    #else errmsg "Namescore and extension score differ in $raw_xccdf"
    else errmsg "Attributes differ in resolved and unresolved file: $raw_xccdf)"
    fi
}




#### MAIN #### 

if [ "$#" -eq "1" ]; then
    case "$1" in
        -h | --help ) usage ;;
        -V | --version ) echo "Version: $version"; exit 0 ;;
        - | -?* ) errmsg 1 "Illegal standalone flag: \"$1\". Please specify parameters accordingy." ;; 
        * ) resolv_check "$1" ;;
    esac

elif [ "$#" -gt "1" ]; then 

    while [ "$1" != "" ]
    do
        case "$1" in
            -v | --verbose ) export vrb=$((++vrb)) ;;
            -vv ) export vrb=$((vrb+2)) ;;
            - | -?* ) errmsg 1 "Illegal flag or context: \"$1\". Please specify parameters accordingly." ;; 
            * ) resolv_check "$1" ;;
        esac
        
        shift
    done

else errmsg 1 "No flags or insufficient parameters have been specified." 
fi












