#!/usr/bin/env bash

# Copyright 2009 Red Hat Inc., Durham, North Carolina.
# All Rights Reserved.
#
# OpenScap Probes Test Suite.
#
# Created on: Nov 30, 2009
#
# Authors:
#      Peter Vrabec, <pvrabec@redhat.com>
#      David Niemoller
#      Ondrej Moris, <omoris@redhat.com>

. ${srcdir}/../../test_common.sh

# Test Cases.

function test_api_probes_smoke {
    ./test_api_probes_smoke
    return $?
}

# Testing.

test_init "test_probes_api.log"

test_run "test_api_probes_smoke" test_api_probes_smoke

test_exit
